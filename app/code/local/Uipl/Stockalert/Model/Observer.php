<?php
class Uipl_Stockalert_Model_Observer {

    public function getalert($observer) {
        
       $order = $observer->getEvent()->getOrder();
       
       $orders = Mage::getModel("sales/order")->load($order->getId());


        $ordered_items = $orders->getAllItems();
        
        foreach($ordered_items as $item){
            
            $product_id =$item->getProductId();
            //product id
           
            $orderqty= $item->getQtyOrdered();
            $_product = Mage::getModel('catalog/product')->load($product_id);  
           //$qtyStock= $_product->getMinQty();
           
           $num= Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getQty();
           $qtyStock=Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getMinQty();
           $qtyStock=$qtyStock-$orderqty;
                        if($num<=$qtyStock){
                                 $email_to=Mage::getStoreConfig('trans_email/ident_sales/email');
                                 $template_id=2;
                                 
                                 
                                 
                                 $email_template_variables = array(
                                 'title' => $_product->getName(),
                                 'id' => $_product->getId(),
                                 'sku' => $_product->getSku(),
                                 'url' => $_product->getProductUrl(),
                                 
                                 );
                                 
                                 
                                 $sender_name = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
                                 
                                 $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
                                 
                                 
                                 $sender = array('name' => $sender_name,
                                 'email' => $sender_email);
                                 
                                 $store = Mage::app()->getStore()->getId();
                                 $translate  = Mage::getSingleton('core/translate');
                                 
                                 Mage::getModel('core/email_template')
                                 ->sendTransactional($template_id, $sender, $email_to, 'KTMKING Admin', $email_template_variables, $storeId);
                                 
                                 $translate->setTranslateInline(true);
                         
                        }
           
           
            //Mage::log('pid='. $product_id." sale qty =".$orderqty." stock qty=".$qtyStock." another qty=".$num.$processedTemplate);
          
            }        
       
    }

}
?>