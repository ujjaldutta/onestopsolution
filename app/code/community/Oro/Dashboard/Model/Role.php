<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Model_Role extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('oro_dashboard/role');
    }

    /**
     * Get dashboard role ids filtered by field/value
     * @param  int    $dashboardId
     * @param  string $field
     * @param  string $value
     * @return array
     */
    public function getRoleIds($dashboardId, $field, $value)
    {

        $collection = $this->getCollection()->addFieldToFilter("dashboard_id", $dashboardId)
            ->addFieldToFilter($field, $value);
        $roleIds = array();

        foreach ($collection as $item) {
            $roleIds[] = $item->getUserRoleId();
        }

        return $roleIds;
    }
}
