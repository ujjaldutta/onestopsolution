<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Model_Resource_Role extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('oro_dashboard/permissions_role', 'id');
    }

    /**
     * Remove roles for dashboard
     *
     * @param  int  $dashboardId
     * @return null
     */
    public function remove($dashboardId)
    {
        $condition = array(
            'dashboard_id = ?' => (int) $dashboardId,
        );

        $this->_getWriteAdapter()->delete($this->getMainTable(), $condition);
    }
}
