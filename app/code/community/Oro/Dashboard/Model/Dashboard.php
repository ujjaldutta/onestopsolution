<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Model_Dashboard extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('oro_dashboard/dashboard');
    }

    /**
     * Check if user is allowed to edit dashboard
     *
     * @param  int     $userId
     * @return boolean
     */
    public function canEdit($userId)
    {
        if (Mage::helper('oro_dashboard')->canManageDashboards()) { //administrator can view/edit all dashboards
            return true;
        }

        return $this->getResource()->canEdit($this->getId(), $userId);
    }

    /**
     * Check if user is allowed to view dashboard
     *
     * @param  int     $userId
     * @return boolean
     */
    public function canView($userId)
    {
        if (Mage::helper('oro_dashboard')->canManageDashboards()){ //administrator can view/edit all dashboards
            return true;
        }

        return $this->getResource()->canView($this->getId(), $userId);
    }

    /**
     * Processing object before delete data, removing widgets associated with dashboard
     *
     * @return Oro_Dashboard_Model_Dashboard
     */
    protected function _beforeDelete()
    {
        Mage::getResourceModel("oro_dashboard/widget")->removeDashboardWidgets($this->getId());
        parent::_beforeDelete();
    }
}
