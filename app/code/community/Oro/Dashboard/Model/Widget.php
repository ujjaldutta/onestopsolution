<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Model_Widget extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('oro_dashboard/widget');
    }

    /**
     * Assign widget to dashboard
     * @param  int                        $dashboardId
     * @return Oro_Dashboard_Model_Widget
     */
    public function assignToDashboard($dashboardId)
    {
        $this->getResource()->assignToDashboard($this->getId(), $dashboardId);

        return $this;
    }

    /**
     * Get Widget config array
     *
     * @return array
     */
    public function getRawWidgetConfig()
    {
        $widgetConfig = new Varien_Object();
        $widgetConfig->setData(Mage::helper('core')->jsonDecode($this->getWidgetConfig()));

        return $widgetConfig;
    }
}
