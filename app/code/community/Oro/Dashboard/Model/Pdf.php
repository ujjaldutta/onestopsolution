<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Model_Pdf extends Mage_Core_Model_Abstract
{
    /**
     * Write image of widget to file
     *
     * @param string $img
     * @param int $currentIteration
     * @param string $widgetName
     * @throws Exception
     */
    public function writeTmpPdfImage($img, $currentIteration, $widgetName)
    {
        $img = base64_decode(substr($img, strpos($img, ',') + 1));
        $path = Mage::getBaseDir('var').DS.'tmppdf';
        $hash = md5($img.$currentIteration);
        $fullPath = $path.'/'.$hash.'.png';
        $ioFile = new Varien_Io_File();
        $ioFile->setAllowCreateFolders(true);
        $ioFile->open(array('path' => $path));
        $ioFile->streamOpen($fullPath);
        $ioFile->streamWrite($img);
        $ioFile->streamClose();

        $this->writeFilePathToSession($currentIteration, $widgetName, $fullPath);
    }

    /**
     * Write saved file paths to session
     *
     * @param int $currentIteration
     * @param string $widgetName
     * @param string $fullPath
     */
    public function writeFilePathToSession($currentIteration, $widgetName, $fullPath)
    {
        $session = Mage::getSingleton('core/session');

        if ($currentIteration == 1) {
            $session->setDashboardGeneratedImagesArray(array(array('widgetName' => $widgetName, 'src'=>$fullPath)));
        } elseif ($currentIteration > 1) {
            $savedImages = $session->getDashboardGeneratedImagesArray();

            if (is_array($savedImages)) {
                $savedImages[]=array('widgetName' => $widgetName, 'src'=>$fullPath);
            } else {
                $savedImages = array('widgetName' => $widgetName, 'src'=>$fullPath);
            }

            $session->setDashboardGeneratedImagesArray($savedImages);
        }
    }
}