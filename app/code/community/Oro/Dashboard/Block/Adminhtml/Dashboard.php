<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Block_Adminhtml_Dashboard extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_dashboard';
        $this->_blockGroup = 'oro_dashboard';
        parent::__construct();

        $this->_headerText = Mage::helper('oro_dashboard')->__('Manage Dashboards');
        $this->_updateButton('add', 'label', Mage::helper('oro_dashboard')->__('Add New Dashboard'));

        if (!Mage::helper('oro_dashboard')->isSectionAllowed('dashboards_create')) {
            $this->_removeButton('add');
        }
    }
}
