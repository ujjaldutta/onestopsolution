<?php
/**
* MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
*
* @category   MageCore
* @package    Dashboard
* @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
* @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/

class Oro_Dashboard_Block_Adminhtml_Userdashboard_Widget_Timeline 
    extends Oro_Dashboard_Block_Adminhtml_Userdashboard_Widget_Abstract
{
    protected function _prepareLayout()
    {
        $this->setTemplate('oro_dashboard/widget/timeline.phtml');
        return parent::_prepareLayout();
    }

    /**
     * Get widget metric compare
     *
     * @return string
     */
    public function getMetricCompare()
    {
        return $this->getWidgetConfig()->getMetricCompare();
    }

    /**
     * Get widget data
     *
     * @return string
     */
    public function getWidgetData()
    {
        /** @var $helper Oro_Dashboard_Helper_Data */
        $helper = Mage::helper('oro_dashboard');
        $widgetData = '';
        $metricClass = $helper->getMetricsClass($this->getMetric());
        $lineMain = array();
        $lineCompare = array();

        $dataFull = array();
        $dataFullCompare = array();

        $isHourlyPlot = $helper->isHourlyPlot();
        if ($metricClass) {
            $metric = new $metricClass;
            if ($data = $metric->getDataForTimeline()) {
                if ($isHourlyPlot) {
                    foreach ($data as $point) {
                        if ($point['count'] != 0) {
                            $this->_valuesArray['main'][] = (int)$point['count'];
                        }
                        $lineMain[] = "['" . $helper->getStoreDate($point['period_grouped']) . "'," . (int)$point['count'] . "]";
                        $dataFull[$helper->getStoreDate($point['period_grouped'])] = "['" . $helper->getStoreDate($point['period_grouped']) . "'," . (int)$point['count'] . "]";
                    }
                } else {
                    foreach ($data as $point) {
                        if ($point['count'] != 0) {
                            $this->_valuesArray['main'][] = (int)$point['count'];
                        }
                        $lineMain[] = "['" . $point['period_grouped'] . "'," . (int)$point['count'] . "]";
                        $dataFull[$point['period_grouped']] = "['" . $point['period_grouped'] . "'," . (int)$point['count'] . "]";
                    }
                }

                $lineMain = implode(",", $lineMain);
                $dataFull = implode(",", $dataFull);
            }
        }
        if ($this->getMetricCompare()) {
            $metricClass = $helper->getMetricsClass($this->getMetricCompare());
            if ($metricClass) {
                $metric = new $metricClass;
                if ($data = $metric->getDataForTimeline()) {
                    if ($isHourlyPlot) {
                        foreach ($data as $point) {
                            if ($point['count'] != 0) {
                                $this->_valuesArray['compare'][] = (int)$point['count'];
                            }
                            $lineCompare[] = "['" . $helper->getStoreDate($point['period_grouped']) . "'," . (int)$point['count'] . "]";
                            $dataFullCompare[$helper->getStoreDate($point['period_grouped'])] = "['" . $helper->getStoreDate($point['period_grouped']) . "'," . (int)$point['count'] . "]";
                        }
                    } else {
                        foreach ($data as $point) {
                            if ($point['count'] != 0) {
                                $this->_valuesArray['compare'][] = (int)$point['count'];
                            }
                            $lineCompare[] = "['" . $point['period_grouped'] . "'," . (int)$point['count'] . "]";
                            $dataFullCompare[$point['period_grouped']] = "['" . $point['period_grouped'] . "'," . (int)$point['count'] . "]";
                        }
                    }
                    $lineCompare = implode(",", $lineCompare);
                    $dataFullCompare = implode(",", $dataFullCompare);
                }
            }
        }
        if ($lineMain) {
            // $widgetData = "[" . $lineMain . ($lineCompare ? "],[" . $lineCompare . "]" : "]");
            $widgetData = "[" . $dataFull . ($dataFullCompare ? "],[" . $dataFullCompare . "]" : "]");
        } else {
            $widgetData = '';
        }

        return $widgetData;
    }
}
