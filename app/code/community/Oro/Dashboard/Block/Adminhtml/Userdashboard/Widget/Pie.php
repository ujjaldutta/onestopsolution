<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Block_Adminhtml_Userdashboard_Widget_Pie extends Oro_Dashboard_Block_Adminhtml_Userdashboard_Widget_Abstract
{
    const SECTORS_LIMIT = 10;

    protected function _prepareLayout()
    {
        $this->setTemplate('oro_dashboard/widget/pie.phtml');

        return parent::_prepareLayout();
    }

    /**
     * Get widget group by attribute
     *
     * @return string
     */
    public function getGroupedByAttribute()
    {
        return $this->getWidgetConfig()->getGroupedByAttribute();
    }

    /**
     * Get pie sectors limit
     *
     * @return int
     */
    public function getSectorsNumber()
    {
        return $this->getWidgetConfig()->getSectorsNumber();
    }

    /**
     * Get widget data
     *
     * @return string
     */
    public function getWidgetData()
    {
        /** @var $helper Oro_Dashboard_Helper_Data */
        $helper = Mage::helper('oro_dashboard');
        $widgetData = '';
        $metricClass = $helper->getMetricsClass($this->getMetric());
        if ($metricClass) {
            $metric = new $metricClass;
            $data = $metric->getDataForPie($helper->getAttributeData($this->getGroupedByAttribute()), ($this->getSectorsNumber() - 1));
            $allData = $metric->getData();
            $pieData = array();
            $sum = 0;
            if ($data) {
                foreach ($data as $grouped) {
                    $pieData[] = "['" . addslashes($grouped['group_value']) . "'," . (int)$grouped['count'] . "]";
                    $sum += $grouped['count'];
                }
                $pieData[] = "['" . addslashes($this->__("Other")) . "'," . ($allData - $sum) . "]";
                $pieData = implode(",", $pieData);
            }
        }
        if ($pieData) {
            $widgetData = "[" . $pieData . "]";
        } else {
            $widgetData = '';
        }

        return $widgetData;
    }
}
