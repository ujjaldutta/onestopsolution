<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Block_Adminhtml_Userdashboard_Aggregation extends Mage_Adminhtml_Block_Template
{
    protected function _prepareLayout()
    {
        $this->setTemplate('oro_dashboard/aggregation/process.phtml');

        return parent::_prepareLayout();
    }

    /**
     * Get aggregation process url
     *
     * @return string
     */
    public function getAggregationProcessUrl()
    {
        return $this->getUrl('*/*/dailyAggregationProcess');
    }

    /**
     * Get aggregation status url
     *
     * @return string
     */
    public function getAggregationStatusUrl()
    {
        return $this->getUrl('*/*/dailyAggregationStatus');
    }

    /**
     * Get days count for daily aggregation
     *
     * @return string
     */
    public function getDaysCount(){
        $currentDate = new Zend_Date();
        $currentDate->subDay(1);
        $currentDate->setTime("00:00:00");
        $startDate = Mage::helper('oro_dashboard')->getStartDbTime();

        return ceil($currentDate->sub($startDate)->toValue() / 60 / 60 / 24);
    }
}
