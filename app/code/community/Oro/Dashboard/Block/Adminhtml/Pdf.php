<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Block_Adminhtml_Pdf extends Mage_Adminhtml_Block_Template
{
    /**
     * Get files of graphical widgets
     *
     * @return null|string
     */
    public function getGraphicalFiles()
    {
        return Mage::getSingleton('core/session')->getDashboardGeneratedImagesArray();
    }

    /**
     * Get textual widgets
     *
     * @return null|string
     */
    public function getTextualWidgets()
    {
        $widgetsArray = array();

        $userDashboard = $this
            ->getLayout()
            ->getBlock('userdashboard');

        $columnsCount = 3;
        for ($i = 1; $i <= $columnsCount; $i++) {
            $widgets = $userDashboard->getWidgets($i);
            foreach ($widgets as $widget) {
                if (in_array($widget->getRawWidgetConfig()->getType(), array('metric', 'table'))) {
                    $widgetsArray[]=$widget;
                }
            }
        }

        return $widgetsArray;
    }
} 