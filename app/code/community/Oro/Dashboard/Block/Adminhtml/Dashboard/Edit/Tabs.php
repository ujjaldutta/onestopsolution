<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Block_Adminhtml_Dashboard_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('dashboard_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('oro_dashboard')->__('Dashboard Details'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('oro_dashboard')->__('General'),
            'title' => Mage::helper('oro_dashboard')->__('General'),
            'content' => $this->getLayout()->createBlock('oro_dashboard/adminhtml_dashboard_edit_tab_form')->toHtml(),
        ));

        $this->addTab('roles_section', array(
            'label' => Mage::helper('oro_dashboard')->__('Roles'),
            'title' => Mage::helper('oro_dashboard')->__('Roles'),
            'content' => $this->getLayout()->createBlock('oro_dashboard/adminhtml_dashboard_edit_tab_roles')->toHtml(),
        ));

        $this->addTab('users_section', array(
            'label' => Mage::helper('oro_dashboard')->__('Users'),
            'title' => Mage::helper('oro_dashboard')->__('Users'),
            'content' => $this->getLayout()->createBlock('oro_dashboard/adminhtml_dashboard_edit_tab_users')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
