<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Block_Adminhtml_Dashboard_Edit_Grid_Widget_Renderer_Checkbox
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Checkbox
{

    /**
     * Renders header of the column
     *
     * @return string
     */
    public function renderHeader()
    {
        $html = parent::renderHeader();
        if ($this->getColumn()->getColumnLabel()) {
            $html = "<span class='sort-title'>" . $this->getColumn()->getColumnLabel() . "  " . $html . "</span>";
        }

        return $html;
    }
}
