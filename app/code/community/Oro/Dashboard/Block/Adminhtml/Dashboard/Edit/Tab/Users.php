<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Dashboard
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Dashboard_Block_Adminhtml_Dashboard_Edit_Tab_Users extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $dashboardId = $this->getRequest()->getParam('id', false);

        $users = Mage::getModel("admin/user")->getCollection()->load();
        $this->setTemplate('oro_dashboard/dashboard_users.phtml')
            ->assign('users', $users->getItems())
            ->assign('dashboardId', $dashboardId);
    }

    protected function _prepareLayout()
    {
        $this->setChild('userGrid', $this->getLayout()->createBlock('oro_dashboard/adminhtml_dashboard_edit_grid_users', 'dashboardUsersGrid'));

        return parent::_prepareLayout();
    }

    protected function _getGridHtml()
    {
        return $this->getChildHtml('userGrid');
    }

    protected function _getJsObjectName()
    {
        return $this->getChild('userGrid')->getJsObjectName();
    }
}
