<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Analytics
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Analytics_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup
{
    /** Get config timezone from DB
     *
     * @return int
     */
    public function getStoreTimezone()
    {
        $select = $this->getConnection()->select();
        $select->from($this->getTable('core/config_data'), 'value');
        $select->where("scope_id = :scope_id AND path = :path AND scope = :scope");
        $binds = array('scope_id' => 0, 'path' => 'general/locale/timezone',
                       'scope' => Mage_Adminhtml_Block_System_Config_Form::SCOPE_DEFAULT);

        return $this->getConnection()->fetchOne($select,$binds);
    }
}