<?php
/**
 * MageCore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is published at http://opensource.org/licenses/osl-3.0.php.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magecore.com so we can send you a copy immediately
 *
 * @category   MageCore
 * @package    Analytics
 * @copyright  Copyright (c) 2015 MageCore Inc. (http://www.magecore.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Oro_Analytics_Model_Observer
{
    /**
     * Handler for system config save after event
     *
     * @param Varien_Event_Observer $observer
     * @return boolean
     */
    public function actionConfigSaveAfter($observer)
    {
        $savedTimezone = Mage::getStoreConfig('general/locale/timezone');
        $lastTimezone = Mage::getStoreConfig(Oro_Analytics_Helper_Data::XML_CURRENT_STORE_TIMEZONE);
        if ($savedTimezone !== $lastTimezone) {
            Mage::getResourceModel('oro_analytics/dailyAggregation')->truncateTables();
            Mage::getModel('core/config')->saveConfig(Oro_Analytics_Helper_Data::XML_CURRENT_STORE_TIMEZONE,
                $savedTimezone);
        }
    }
}
